//
//  BQTAddTimerViewController.swift
//  BQT_IOT
//
//  Created by SCISPLMAC on 13/08/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit
import CocoaMQTT
import CocoaAsyncSocket

class MQTTConnection: NSObject {
    var mqtt: CocoaMQTT?
    static let sharedInstance = MQTTConnection()
    private override init() {}
    func mqttSetting() -> CocoaMQTT{
        var cid = ""
        if let userId  = UserDefaults.standard.value(forKey: "userId") as? String {
            cid = userId
        }
        let clientID: String = cid
        print("clientID",clientID)
        let mqttServer: String = "13.235.159.5"
        
        let useSSL: Bool = true
        self.mqtt = CocoaMQTT(clientID: clientID, host: mqttServer, port: 1883)
        self.mqtt!.username = "bqt"
        self.mqtt!.password = "bqt@123"
        self.mqtt!.keepAlive = 240
        self.mqtt!.cleanSession = true
        self.mqtt!.autoReconnect = true
        self.mqtt!.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
        if useSSL == true{
            self.mqtt!.enableSSL = false
            self.mqtt!.allowUntrustCACertificate = false
        }
       self.mqtt!.connect()
       return mqtt!
    }
    
}
