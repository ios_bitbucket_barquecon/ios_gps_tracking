//
//  GPSConstatnts.swift
//  TH IOT
//
//  Created by SCISPLMAC on 03/09/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit

class GPSConstatnts: NSObject {
    /*
     * Constants regarding to API Calling
     */
    static let baseURL = "http://52.8.218.210/bqt_iot_php/apis/MobileApis/"
    static let loginAPIConstant = "login"
    static let registerUser = "registerUser"
    static let forgotPassword = "forgotPassword"
    static let getDeviceDetailsByUserId = "getDeviceDetailsByUserId"
    static let getAnalysisData = "getAnalysisData"
    static let renameSwitchKeyName = "renameSwitchKeyName"
    static let setImage = "setImage"
    static let getDeviceDetailsByUserIdGroupBy = "getDeviceDetailsByUserIdGroupBy"
    static let getScheduleBySwitchKey = "getScheduleBySwitchKey"
    static let setScheduleBySwitchKey = "setScheduleBySwitchKey"
    static let deleteDeviceByMacAddress = "deleteDeviceByMacAddress"
    static let saveDeviceToken = "saveDeviceToken"
    static let getRegisterDeviceByUserOrDevice = "getRegisterDeviceByUserOrDevice?deviceType="
    static let macId = "&macAddress="
    static let email = "&email="
    static let noOfPort = "&noOfPort=1"
    //COLOR
    static let redColor = UIColor(red: 255/255.0, green: 86/255.0, blue: 96/255.0, alpha: 1.0)
    static let lightGrayColor = UIColor(red: 189/255.0, green: 189/255.0, blue: 189/255.0, alpha: 1.0)
    static let primaryTextColor = UIColor(red: 33/255.0, green: 33/255.0, blue: 33/255.0, alpha: 1.0)
    static let secondaryTextColor = UIColor(red: 117/255.0, green: 117/255.0, blue: 117/255.0, alpha: 1.0)
    static let accentColor = UIColor(red: 251/255.0, green: 192/255.0, blue: 45/255.0, alpha: 1.0)
    static let lightColor = UIColor(red: 208/255.0, green: 208/255.0, blue: 208/255.0, alpha: 1.0)
}
