//
//  GPSUtilities.swift
//  TH_IOT
//
//  Created by SCISPLMAC on 03/09/19.
//  Copyright © 2019 SCISPLMAC. All rights reserved.
//

import UIKit

class GPSUtilities: NSObject {
     static let sharedInstance = GPSUtilities()
        var isMqttConnected = false
    class func valiadateBlankText(text: String?) -> Bool {
        guard let text = text, text.isEmpty else { return true }
        return false
    }
    
    class func valiadatePhoneNumber(text: String?) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: text)
        return result
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func isPasswordValid(_ password : String?) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[a-z])(?=.*[\\d])[A-Za-z\\d$@$#!%*?&]{6,}$")
        return passwordTest.evaluate(with: password)
    }
    func canOpenAppSettingsAndURL() -> (canOpen:Bool, WiFiString: String) {
        
        if UIApplication.shared.canOpenURL(URL(string: "App-Prefs:root=WIFI")!) {
            return (true, "App-Prefs:root=WIFI")
        } else if UIApplication.shared.canOpenURL(URL(string: "prefs:root=WIFI")!) {
            return (true, "prefs:root=WIFI")
        } else {
            return (true, "App-Prefs:root=WIFI")
        }
    } 
}
