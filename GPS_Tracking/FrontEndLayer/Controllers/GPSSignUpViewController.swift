//
//  GPSSignUpViewController.swift
//  GPS_Tracking
//
//  Created by SCISPLMAC on 05/11/19.
//  Copyright © 2019 BQT. All rights reserved.
//

import UIKit
import Alamofire
class GPSSignUpViewController: GPSBaseViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
       // dismiss(animated: true, completion: nil)
    }
    @IBAction func signUpButtonAction(_ sender: Any) {
        if nameTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter name")
        } else if emailTextField.text!.isEmpty || !GPSUtilities.isValidEmail(testStr: emailTextField.text!) {
            alertUser("Alert", message: "Enter valid email")
        }else if mobileNumberTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter mobile number")
        }else if passwordTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter valid email")
        } else {
            self.signUp()
        }
    }
    @IBAction func passwordHideShowButtonAction(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        passwordTextField.isSecureTextEntry = !(sender as! UIButton).isSelected
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension GPSSignUpViewController {
    func signUp(){
        startAnimatingActivityIndicator()
        let urlString = String(format: "%@%@", GPSConstatnts.baseURL,GPSConstatnts.registerUser)
        print(urlString)
        let param = ["name": nameTextField.text!,
                     "email":emailTextField.text!,
                     "mobileNo":mobileNumberTextField.text!,
                     "password":passwordTextField.text!,
                     "address" : "Pune",
                     "type" : "1"
        ]
        print(param)
        Alamofire.request(urlString, method: .post, parameters: param , encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                self.stopAnimating()
                // print("Jokes_response",response)
                guard let res = response.result.value else {return}
                
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                    let data = (res as AnyObject).value(forKey: "userObject") as! [String: AnyObject]
                   // print(data,"data")
                    self.alertPopToRootController("BQT_IOT", message: message)
                }else {
                    self.alertUser("Alert", message: message)
                }
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
                self.stopAnimatingActivityIndicator()
            }
            
        }
    }
}

extension GPSSignUpViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            emailTextField.becomeFirstResponder()
        } else if textField == emailTextField  {
            mobileNumberTextField.becomeFirstResponder()
        } else if textField == mobileNumberTextField  {
            passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        }
        return true
    }
}
