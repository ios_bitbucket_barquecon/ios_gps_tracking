//
//  GPSLoginViewController.swift
//  GPS_Tracking
//
//  Created by SCISPLMAC on 05/11/19.
//  Copyright © 2019 BQT. All rights reserved.
//

import UIKit
import Alamofire
class GPSLoginViewController: GPSBaseViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
       let num =  randomNumberWith(digits:8)
        let newNum = "98"+String(num)
        print("num",newNum)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        if mobileNumberTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter mobile number")
        } else if passwordTextField.text!.isEmpty {
            alertUser("Alert", message: "Enter password")
        }else {
            login(mobileNumberTextField.text!, password: passwordTextField.text!)
        }
    }
    @IBAction func passwordHideShowButtonAction(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        passwordTextField.isSecureTextEntry = !(sender as! UIButton).isSelected
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "GPSSignUpViewController") as! GPSSignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        let alertController = UIAlertController(title: "Forgot Password?", message: "Please enter your registered email", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Submit", style: .default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            print("firstName \(firstTextField.text!)")
            firstTextField.keyboardType = .emailAddress
            if !GPSUtilities.isValidEmail(testStr: firstTextField.text!){
                self.alertUser("Email", message: "Please Enter valid Email")
            }else {
                self.forgotPassword(firstTextField.text!)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter your registered email"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension GPSLoginViewController {
    func login(_  userName: String , password : String){
        startAnimatingActivityIndicator()
        let token = UserDefaults.standard.value(forKey: "deviceToken") as? String
        let urlString = String(format: "%@%@", GPSConstatnts.baseURL,GPSConstatnts.loginAPIConstant)
        // print(urlString)
        Alamofire.request(urlString, method: .post, parameters: ["mobileNo": userName,"password":password, "fcm_key" : token!,"platform": "iOS"], encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                self.stopAnimating()
                //print("Jokes_response",response)
                guard let res = response.result.value else {return}
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                    let data = (res as AnyObject).value(forKey: "userObject") as! [String: AnyObject]
                    if let userId = data["id"] as? String {
                        UserDefaults.standard.set(userId, forKey: "userId")
                        UserDefaults.standard.synchronize()
                        //  print(userId)
                    }
                    
                    if let name = data["name"] as? String{
                        UserDefaults.standard.set(name, forKey: "name")
                        UserDefaults.standard.synchronize()
                    }
                    if let email = data["email"] as? String{
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.synchronize()
                    }
                    
                    self.getNotification()
                }else  {
                    self.alertUser("Alert", message: message)
                }
                
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
                self.stopAnimatingActivityIndicator()
            }
        }
    }
    func getNotification(){
        startAnimatingActivityIndicator()
        let token = UserDefaults.standard.value(forKey: "deviceToken") as? String
        let userId = UserDefaults.standard.value(forKey: "userId") as? String
        let urlString = String(format: "%@%@", GPSConstatnts.baseURL,GPSConstatnts.saveDeviceToken)
        // print(urlString)
        Alamofire.request(urlString, method: .post, parameters: ["user_id": userId!, "deviceToken" : token!,"platform": "1"], encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                self.stopAnimating()
                print("Jokes_response",response)
                guard let res = response.result.value else {return}
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                    UserDefaults.standard.set(true, forKey: "isLogin")
                    //print("Login Successfully")
                    UserDefaults.standard.synchronize()
                    let dashBoardVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
                    self.navigationController?.pushViewController(dashBoardVC, animated: true)
                }else {
                    self.alertUser("Alert", message: message)
                }
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
                self.stopAnimatingActivityIndicator()
            }
        }
    }
    
    func forgotPassword(_ emailId: String){
        startAnimatingActivityIndicator()
        let urlString = String(format: "%@%@", GPSConstatnts.baseURL,GPSConstatnts.forgotPassword)
        // print(urlString)
        Alamofire.request(urlString, method: .post, parameters: ["email": emailId], encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success :
                self.stopAnimating()
                //print("Jokes_response",response)
                guard let res = response.result.value else {return}
                let status = (res as AnyObject).value(forKey: "status") as! Bool
                 let message = (res as AnyObject).value(forKey: "message") as! String
                if status == true {
                    self.alertUser("", message: message)
                }
            case .failure(let error):
                print(error)
                self.alertUser("Error", message: "Server seems to be down")
                self.stopAnimatingActivityIndicator()
            }
        }
    }
    
    func randomNumberWith(digits:Int) -> Int {
        let min = Int(pow(Double(10), Double(digits-1))) - 1
        let max = Int(pow(Double(10), Double(digits))) - 1
        return Int(Range(uncheckedBounds: (min, max)))
    }
}
extension GPSLoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mobileNumberTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField  {
            passwordTextField.resignFirstResponder()
        }else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        }
        return true
    }
}
extension Int {
init(_ range: Range<Int> ) {
    let delta = range.lowerBound < 0 ? abs(range.lowerBound) : 0
    let min = UInt32(range.lowerBound + delta)
    let max = UInt32(range.upperBound   + delta)
    self.init(Int(min + arc4random_uniform(max - min)) - delta)
    }
}
